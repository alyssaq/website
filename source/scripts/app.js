import Choreographer from './choreographer.js'

import './components/cli-help.js'
import './components/link.js'
import './components/layout.js'
import './components/authentication/login.js'
import './components/authentication/signup.js'
import './components/authentication/password-reset.js'
import './components/authentication/password-reset-sent.js'
import './components/sites/list.js'
import './components/sites/details.js'
import './components/sites/add.js'
import './components/account/profile.js'
import './components/account/logged-out.js'
import './components/404.js'

const choreographer = new Choreographer({
  stage: document.body,
  scenes: [
    ['/login', 'app-login'],
    ['/signup', 'app-signup'],
    ['/password-reset', 'app-password-reset'],
    ['/password-reset-sent', 'app-password-reset-sent'],
    ['/sites', 'app-sites-list'],
    // ['/sites{/domain}/', 'app-sites-details'],
    ['/sites{/domain}/', 'app-sites-add'], // TODO replace me!
    ['/add', 'app-sites-add'],
    ['/account', new URL('/account/profile', location)],
    ['/account/profile', 'app-account-profile'],
    ['/kthxbye', 'app-account-logged-out'],
    ['/{pathname}', 'app-404']
  ]
})

export default { choreographer }
