import TemplateElement from './TemplateElement.js'

const originalTitle = document.title

function setTitle (message) {
  if (typeof message === 'string' && message.length > 0) {
    document.title = `${message} - ${originalTitle}`
  } else {
    document.title = originalTitle
  }
}

function menuItem (template, url, label, active) {
  const fragment = document.importNode(template.content, true)
  const anchor = fragment.querySelector('a')
  anchor.setAttribute('href', url)
  anchor.textContent = label
  if (active === true) {
    fragment.querySelector('li').classList.add('active')
  }
  return fragment
}

class Layout extends TemplateElement {
  constructor () {
    super()
    this.template = '/scripts/components/layout.html'
  }

  static get observedAttributes () {
    return ['subtitle']
  }

  attributeChangedCallback (name, oldValue, newValue) {
    if (name === 'subtitle') {
      setTitle(newValue)
    }
  }

  async connectedCallback () {
    await super.connectedCallback()

    setTitle(this.getAttribute('subtitle'))

    const root = this.shadowRoot
    const template = root.querySelector('#menu-item')

    const isAuthenticated = localStorage.getItem('app.accessToken') !== null

    if (!isAuthenticated) {
      root.querySelector('#topics').setAttribute('hidden', true)
    }

    const authentication = root.querySelector('#authentication')
    if (isAuthenticated) {
      authentication.appendChild(
        menuItem(
          template,
          '/account',
          'Account',
          location.pathname.startsWith('/account')
        )
      )
      root.querySelector('#topics').appendChild(
        menuItem(
          template,
          '/sites',
          'Sites',
          location.pathname.startsWith('/sites') ||
          location.pathname === '/add'
        )
      )
      if (location.pathname.startsWith('/sites/')) {
        const domain = decodeURIComponent(location.pathname.split('/')[2])
        const link = document.createElement('a')
        link.href = `/sites/${encodeURIComponent(domain)}/`
        link.textContent = domain
        root.querySelector('#topics > li').appendChild(link)
      }
    } else {
      authentication.appendChild(
        menuItem(template, '/signup', 'Sign Up')
      )
      authentication.appendChild(
        menuItem(template, '/login', 'Log In')
      )
    }

    for (const anchor of root.querySelectorAll('a[href]')) {
      if (location.href === anchor.href) {
        anchor.closest('li').classList.add('active')
      }
    }

    root.querySelector('#brand')
      .addEventListener('click', (event) => {
        event.stopPropagation()
      })
  }
}

window.customElements.define('app-layout', Layout)
export default Layout
