module.exports = () => ({
  https: {
    port: 4433
  },
  http: {
    redirect: false
  },
  hosts: [
    {
      directories: {
        trailingSlash: 'never'
      },
      fallback: {
        200: 'app.html'
      },
      manifest: [
        {
          get: '/index.html',
          push: [
            '/{fonts,images,styles}/**/*',
            '!**/*.map'
          ]
        },
        {
          get: '/app.html',
          push: [
            '/styles/{app,fonts,configuration}.css',
            '/{fonts,images,scripts}/**/*',
            '!**/*.map'
          ]
        }
      ]
    }
  ]
})
